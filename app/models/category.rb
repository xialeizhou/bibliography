class Category < ActiveRecord::Base
  extend ActsAsTree::TreeWalker
  acts_as_tree order: "ddc"

  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]  

  has_and_belongs_to_many :books
  before_destroy { books.clear }

  validates :name, presence: true, :uniqueness => { scope: :ddc, message: 'This category already exists'}
  validates :ddc, presence: true    
  validates_with ParentValidator

  include PgSearch
  pg_search_scope :search_by_name, 
    :against => [
      [:name, 'A'],
      [:ddc, 'B'],
      [:tsearch_vector, 'C']
    ], 
    :using => {
      :tsearch => {:prefix => true, :tsvector_column => :tsearch_vector},
      :trigram => {:threshold => 0.15}
    }, 
    :ignoring => :accents

  after_validation :calculate_search_terms, :if => :recalculate_search_terms?

  # Try building a slug based on the following fields in
  # increasing order of specificity.
  def slug_candidates
    [
      :slugged_name,
      [:slugged_name, :id],
    ]
  end  

  def slugged_name(opts={})
    opts[:max_expansions] ||= 1
    opts[:dashes] = true if opts[:dashes].nil?
    join_with = opts[:dashes] ? '-' : ' '

    converter = Greeklish.converter(max_expansions: opts[:max_expansions], generate_greek_variants: false)     
    name_to_slug = ApplicationController.helpers.detone(UnicodeUtils.downcase(name).gsub('ς','σ').gsub(/[,.:'·-]/,''))
    name_to_slug.split(" ").map do |word|
      converted = converter.convert(word)
      converted.present? ? converted : word
    end.flatten.uniq.join(join_with)
  end  

  def calculate_search_terms
    write_attribute(:tsearch_vector, slugged_name(max_expansions: 3, dashes: false) + ' ' + ddc)    
  end  

  def recalculate_search_terms?
    :name_changed? || :ddc_changed?
  end  
    
end
