# Files in the config/locales directory are used for internationalization
# and are automatically loaded by Rails. If you want to use locales other
# than English, add the necessary files in this directory.
#
# To use the locales, use `I18n.t`:
#
#     I18n.t 'hello'
#
# In views, this is aliased to just `t`:
#
#     <%= t('hello') %>
#
# To use a different locale, set it with `I18n.locale`:
#
#     I18n.locale = :es
#
# This would use the information in config/locales/es.yml.
#
# To learn more, please read the Rails Internationalization guide
# available at http://guides.rubyonrails.org/i18n.html.

en:
  welcome: "Welcome to Bibliography GR"
  some_options: "Here are some options to get started"
  add_books: "Add Books"
  browse_books: "Browse books"
  add_books_to_library: "Add books to your library"
  spruce_up_profile: "Spruce up your profile"
  edit_profile: "Edit your profile"
  add_member_photo: "Add a member photo"
  engage: "Engage with others"
  read_reviews: "Read member reviews"
  invite_friends: "Invite friends"
  and: "and"

  home: "Home"
  collections: "Collections"
  my_books: "My Books"
  my_collections: "My Collections"
  recently_added: "Recently Added"

  site_settings: "Site Settings"
  add_to_collection: "Add to collection"
  please_login: "Please Login"
  login: "Log in"
  signup: "Sign Up"
  create_account: "Create new account"
  forgot_password: "Forgot your password?"
  password: "Password"
  remember_me: "Remember me"
  register: "Register"
  create_account: "Create Account"
  password_confirmation: "Password confirmation" 
  enter_email: "Enter your email"
  enter_password: "Enter your password"
  confirm_password: "Confirm your password"
  terms: "Terms"
  agree_to_terms: "Agree to terms"
  back_to_login: "Back to login"
  users: "Users"
  logout: "Logout"
  account_settings: "Account settings"

  search: "Search"
  write_search_terms: "Write search terms..."
  nothing_found: "Nothing was found..."

  search_my_books: "Search my books"
  search_results: "Search Results"

  read_more: "Read More"
  read_less: "Close"

  stats:
    views: "Views"
    visitors: "Visitors"

  notifications:
    changes_saved_successfully: "Changes were successfully saved!"
    changed_saved_failed: "Something went wrong. Changes failed to save"

  side_menu:
    discover: "Discover"
    categories: "Categories"
    books: "Books"
    authors: "Authors"
    publishers: "Publishers"

    featured: "Featured"
    trending: "Trending"
    latest: "Latest"
    awarded: "Awarded"

    editors_only: "Editors Only"
    contributions: "Contributions"
    awards: "Awards"
    prizes: "Prizes"

    admins_only: "Admins Only"
    import: "Import"
    monitoring: "Monitoring"
    tasks: "Tasks"
    api: "API"

  comments:
    label: "Comments"
    or_reviews: "Comments/Reviews"
    sign_in_to_post: "Sign in to post comments/reviews"
    write: "Write your comment.."
    post: "Post"

  show: "Show"
  edit: "Edit"
  destroy: "Destroy"
  cancel: "Cancel"
  save : "Save"
  back: "Back"
  u_sure: "Are you sure?"

  name: "Name"
  privacy: "Privacy"
  active: "Active"
  settings: "Settings"
  language: "Language"

  categories:
    label: "Categories"
    new: "New Category"
    edit: "Edit Category"
    delete: "Delete Category"
    new_subcategory: "New Subcategory"
    editing: "Editing Category"
    listing: "Listing Categories"  
    all: "All Categories"
    search: "Search Categories"
    favoured: "Favoured"
    add_to_favourites: "Add to favourites"   

  authors:
    label: "Authors"
    new: "New Author"
    edit: "Edit Author"
    delete: "Delete Author"
    editing: "Editing Author"
    listing: "Listing Authors"  
    all: "All Authors"
    favoured_x_times: "Favoured %{count} times"
    x_books: "%{count} books"
    with_x_views: "%{count} views"    
    search: "Search Authors"
    favoured: "Favoured"
    add_to_favourites: "Add to favourites"
    main: "Main Author"
    other: "Other Authors"

  publishers:
    label: "Publishers"
    new: "New Publisher"
    edit: "Edit Publisher"
    delete: "Delete Publisher"
    editing: "Editing Publisher"
    listing: "Listing Publishers"  
    all: "All Publishers"
    x_books: "%{count} books"
    with_x_views: "%{count} views"
    search: "Search Publishers"    

  places:
    address: "Address"
    telephones: "Telephones"
    fax: "Fax"
    email: "Email"
    website: "Website"

  books:
    index:
      listing_books: "Listing Books"

    label: "Books"
    new: "New Book"
    edit: "Edit Book"

    title: "Title"
    subtitle: "Subtitle"
    publisher: "Publisher"
    publication: "Publication"
    image: "Image"
    isbn: "Isbn"
    series: "Series"
    price: "Price"
    language: "Language"
    physical_desc: "Physical desc."
    availability: "Availability"
    format: "Format"
    original: "Original"
    original_title: "Original Title"
    original_language: "Original Language"
    genres: "Genres"
    pages: " pages"
    centimeters: "cm"
    last_updated: "Last Updated at"
    description: "Description"
    marc_view: "MARC View"
    isbd_view: "ISBD View"
    isbn: "ISBN"
    isbn13: "ISBN-13"
    ismn: "ISMN"
    issn: "ISSN"
    collective_work: "Collective work"
    n_version: "th revision"
    add: "Add Book"
    added: "Added"
    recommend: "Recommend"
    not_recommend: "Not recommend"
    remove_recommendation: "Remove Recommendation"
    recommended: "Recommended"
    recommended_x_times: "Recommended %{count} times"
    popular: "Popular"
    awarded: "Awarded"
    latest: "Latest" 
    in_x_collections: "In %{count} Collections"
    with_x_views: "%{count} views"
    search: "Search Books"

  import:
    from_file: "From File"
    from_url: "From URL"
    from_text: "From Text"    
    import_data: "Import Data"
    import_records: "Import Records"
    import_label: "Import"
    from_url_hint: "Insert url with data to import"
    from_text_hint: "Write/Paste data to import"
    import_from_file: "Import data from file:"
    import_from_url: "Import data from url:"
    import_from_text: "Import data from text:"
    records_importing_in_bg: "Records are analysed and being imported in background thread."
    danger_zone: "Danger Zone"
    import_from_storage: "Import from storage"
    storage_import_description: "Be careful! All json files inside tmp/storage will be imported."

  shelves:
    all_collections: "All Collections"
    add_to_collections: "Add to collections"
    edit_collections: "Edit collections"
    new: "New Collection"

  users:
    follow: "Follow"
    unfollow: "Unfollow"    
    following: "Following"

  awards:
    label: "Awards"
    new: "New Award"

  # Authorization
  pundit:
    default: 'You cannot perform this action.'
    category_policy:
      update?: 'You cannot edit this category!'
      create?: 'You cannot create category!'
      show?: 'You cannot see that category!'
      destroy?: 'You cannot delete that category!'

  account:
    label: 'Account'
    edit: 'Edit Account'
    user_edit: 'Edit User'
    cancel: 'Cancel Account'
    api_token: 'Api Token'
    api_key: 'Api Key'
    personal_info: 'Personal Information'    
    notifications: 'Notifications'
    receive_notifications: 'Receive email notifications.'
    want_notifications: 'I want to receive notifications!'

  # Pagination
  views:
    pagination:
      first: "&laquo;"
      last: "&raquo;"
      previous: "&lsaquo;"
      next: "&rsaquo;"
      truncate: "&hellip;"
  helpers:
    page_entries_info:
      one_page:
        display_entries:
          zero: "No %{entry_name} found"
          one: "Displaying <b>1</b> %{entry_name}"
          other: "Displaying <b>all %{count}</b> %{entry_name}"
      more_pages:
        display_entries: "Displaying %{entry_name} <b>%{first}&nbsp;-&nbsp;%{last}</b> of <b>%{total}</b> in total"

  languages:
    greek: "greek"