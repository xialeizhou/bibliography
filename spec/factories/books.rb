FactoryGirl.define do
  factory :book do
    title "MyString"
subtitle "MyString"
description "MyText"
image "MyString"
isbn "MyString"
isbn13 "MyString"
ismn "MyString"
issn "MyString"
series "MyString"
pages 1
publication_year 1
publication_place "MyString"
price "9.99"
price_updated_at "2014-12-31"
physical_description "MyString"
cover_type 1
availability 1
format 1
original_language 1
original_title "MyString"
publisher nil
extra "MyString"
biblionet_id 1
  end

end
